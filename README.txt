=== SB-CodeSnippets ===
Contributors: ladislav.soukup@gmail.com
Donate link: https://www.paypal.com/cgi-bin/webscr?cmd=_donations&business=P6CKTGSXPFWKG&lc=CZ&item_name=Ladislav%20Soukup&item_number=SB%20CodeSnippets%20%5bWP%2dPlugin%5d&currency_code=EUR&bn=PP%2dDonationsBF%3abtn_donate_SM%2egif%3aNonHosted
Tags: code, snippets, shortcode, builder
Requires at least: 3.3.1
Tested up to: 3.7
Stable tag: 1.0.0

...

== Description ==
...

== Installation ==
1. Upload to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress
3. Feed is now updated, you should check it


== Changelog ==
= 1.0 =

first release version