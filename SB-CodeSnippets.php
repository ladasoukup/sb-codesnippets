<?php
/*
Plugin Name: SB-CodeSnippets
Plugin URI: http://git.ladasoukup.cz/sb-codesnippets
Description: 
Version: 1.0.0
Author: Ladislav Soukup (ladislav.soukup@gmail.com)
Author URI: http://ladasoukup.cz/
Author Email: ladislav.soukup@gmail.com
License:

  Copyright 2013 Ladislav Soukup (ladislav.soukup@gmail.com)

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License, version 2, as 
  published by the Free Software Foundation.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
  
*/

class SB_CodeSnippets {
	private $plugin_path;
	public $cfg_version = '1.0';
	
	/*--------------------------------------------*
	 * Constructor
	 *--------------------------------------------*/
	
	/**
	 * Initializes the plugin by setting localization, filters, and administration functions.
	 */
	function __construct() {
		$this->plugin_path = plugin_dir_path( __FILE__ );
		
		load_plugin_textdomain( 'SB_CodeSnippets', false, dirname( plugin_basename( __FILE__ ) ) . '/lang' );
		
		add_action( 'init', array( 'SB_CodeSnippets' , 'setup_cpt' ) );
		add_filter( 'cmb_meta_boxes', array( 'SB_CodeSnippets' , 'setup_cmb' ) );
		add_action( 'init', array( 'SB_CodeSnippets' , 'load_cmb_helper' ), 9999 );
		
		add_shortcode( 'snippet', array( 'SB_CodeSnippets', 'sh_snippet' ) );
		
	} // end constructor
	
	public function sh_snippet ( $atts, $content ){
		global $SB_CodeSnippets;
		$out = '';
		$post_title = sanitize_text_field( $atts['id'] );
		
		$snippet_args = array (
			'posts_per_page'  => 1,
			'post_type'       => 'cpt_SB_CodeSnippets',
			'title'			  => $post_title
		);
		$snippet = get_posts( $snippet_args );
		$snippet = $snippet[0];
		
		$snippet_code = get_post_meta( $snippet->ID, 'snippet_code', true );
		
		$out = $snippet_code;
		
		/* DEBUG / ob_start(); echo '<pre>'; print_r( $snippet ); echo '<hr>'; print_r( $atts ); echo '</pre>'; $out .= ob_get_clean(); /**/
		
		return $out;
	}
	
	function load_cmb_helper() {
		global $SB_CodeSnippets;
		
		if ( ! class_exists( 'cmb_Meta_Box' ) )
			require_once $SB_CodeSnippets->plugin_path . 'cmb/init.php';
	}
	
	function setup_cmb( array $meta_boxes ) {
		global $SB_CodeSnippets;
		
		$meta_boxes[] = array(
			'id'         => 'SB_CodeSnippets_cmb_code',
			'title'      => __( 'Snippet code', 'SB_CodeSnippets' ),
			'pages'      => array( 'cpt_SB_CodeSnippets' ),
			'context'    => 'normal',
			'priority'   => 'high',
			'show_names' => true,
			'fields' => array(
				array(
					'name' => __( 'Snippet code', 'SB_CodeSnippets' ),
					'desc' => 'field description (optional)',
					'id'   => 'snippet_code',
					'type' => 'textarea_code'
				),
				array(
					'name' => '[snippet id="%%name%%" %%vars%% /]',
					'desc' => '',
					'id'   => 'snippet_title1',
					'type' => 'title'
				)
			)
		);
		
		return $meta_boxes;
	}
	
	function setup_cpt() {
		global $SB_CodeSnippets;
		register_post_type( 'cpt_SB_CodeSnippets',
			array(
				'labels' => array(
					'name' => __('Code Snippets', 'SB_CodeSnippets' ),
					'singular_name' => __( 'Code Snippet', 'SB_CodeSnippets' ),
					'add_new' => __( 'Add new snippet', 'SB_CodeSnippets' ),
					'add_new_item' => __( 'Add new snippet', 'SB_CodeSnippets' ),
					'all_items' => __( 'All snipets', 'SB_CodeSnippets' )
				),
				'public' => true,
				'has_archive' => true,
				'rewrite' => array( 'slug' => 'code_snippet' ),
				'supports' => array( 'title' ) ,
				'exclude_from_search' => true
			)
		);
		
	}
	
	/**
	 * Fired when the plugin is activated.
	 *
	 * @param	boolean	$network_wide	True if WPMU superadmin uses "Network Activate" action, false if WPMU is disabled or plugin is activated on an individual blog 
	 */
	public function activate( $network_wide ) {
		flush_rewrite_rules();
	} // end activate
	
	/**
	 * Fired when the plugin is deactivated.
	 *
	 * @param	boolean	$network_wide	True if WPMU superadmin uses "Network Activate" action, false if WPMU is disabled or plugin is activated on an individual blog 
	 */
	public function deactivate( $network_wide ) {
		flush_rewrite_rules();
	} // end deactivate
	
	/**
	 * Fired when the plugin is uninstalled.
	 *
	 * @param	boolean	$network_wide	True if WPMU superadmin uses "Network Activate" action, false if WPMU is disabled or plugin is activated on an individual blog 
	 */
	public function uninstall( $network_wide ) {
		
	} // end uninstall
	
	
} // end class
$SB_CodeSnippets = new SB_CodeSnippets();